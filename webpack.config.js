// require path for streamlining paths across operating systems
const path = require("path");
const HTMLWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    /**
     * used to control source maps
     * a variety of options are available which affect performance to different degrees
     * common selections: 
     *  cheap-eval-source-map
     *  cheap-module-source-map
     */
    devtool: 'cheap-module-source-map',
    /**
     * sets the context for which the paths can be derived from
     * this means that the entry paths will be relative to this path
     */
    context: path.join(__dirname, "src"),
    /**
     * Specify the entry point in the application. The entry point can either be a single path or an array of entry points. 
     * Webpack begins from these points and builds a dependency tree
     * 
     * Multiple entry points: create object where you specify a key for the name in webpack along with a path to that file
     */
    entry: {
        app: './app/app.js',
        about: './about/about.js'
    },
    /**
     * filename: [name] command will specify the bundle files to create based on the names of the input
     */
    output: {
        path: path.join(__dirname, "dist"),
        filename: "[name].bundle.js"
    },
    /**
     * object with a number of properties -- one of which being the loaders
     */
    module: {
        /**
         * array of objects where each is a loader
         * loader arguments -
         *  test: regular expression where all files which match have the loader executed against
         *  loader: the specified loader to process the files through
         *  include: files to include. NOTE: specifying an include automatically defines exclusions as well
         *  exclude: files to exclude
         */
        loaders: [
            {
                /**
                 * need to set test as it tells webpack when to use this loader: 
                 *      Test files against this and use this loader on them if a file passes
                 * Uses regular expressions for testing
                 * test(s):
                 *      /\.js$/ - all files ending in .js
                 */
                test: /\.js$/,
                loader: 'babel-loader',
                include: path.join(__dirname, 'src')
            }
        ]
    },
    /**
     * configure devServer from here
     *      will default to root directory from localhost without configuration
     * hot module reloading set up here
     *      reload only that file dynamically while working with it
     *      need reloader for it to work in that way, not built in by default
     * 
     * devServer arguments - 
     *  contentBase: specifies the base to load content from
     *  inline: accepts boolean describing whether or not to remove wrapping iframe
     *  stats: default behavior specifies that almost all build info is output to the terminal, this tells it to limit it based on the provided argument
     */
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        inline: true,
        stats: 'errors-only'
    },
    /**
     * takes an array of plugins which are constructor calls to the plugin
     *      plugins need to be imported in
     */
    plugins: [
        /**
         * by default injects scripts into the body
         * set template to define for what will be loaded
         * 
         * other configurable properties
         *  template - specify template to inject to so that you are not using a blank html page
         *  inject - compiled scripts are automatically thrown into the body by default, this allows you to specify otherwise
         *  hash - used for cachebusting, hash files to filename and automatically inject them
         *  filename - used to specify additional entry point
         *  chunk - use to specify how to break up the bundles
         */
        new HTMLWebpackPlugin({
            template: path.join(__dirname, 'src', 'index.html'),
            hash: true,
            chunks: ['app']
        }),
        new HTMLWebpackPlugin({
            template: path.join(__dirname, 'src', 'index.html'),
            hash: true,
            filename: 'about.html',
            chunks: ['about']
        })
    ]
};