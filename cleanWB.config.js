const path = require("path");
const HTMLWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    devtool: 'cheap-module-source-map',
    context: path.join(__dirname, "src"),
    entry: {
        app: './app/app.js',
        about: './about/about.js'
    },
    output: {
        path: path.join(__dirname, "dist"),
        filename: "[name].bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: path.join(__dirname, 'src')
            }
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        inline: true,
        stats: 'errors-only'
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: path.join(__dirname, 'src', 'index.html'),
            hash: true,
            chunks: ['app']
        }),
        new HTMLWebpackPlugin({
            template: path.join(__dirname, 'src', 'index.html'),
            hash: true,
            filename: 'about.html',
            chunks: ['about']
        })
    ]
};